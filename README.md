# My Archery Training

My archery Training is a web manager tool to write some information for a training of archery.

## Project status
this project is under development

## Installation
not usable for the moment, but a docker image will be provided

## Support
Report any issues in the issue tab

## Road map
### What will be added
list of features planned to be implemented

- [ ] User panel
- [ ] Admin panel to add users and trainers
- [ ] Trainers panel to view the users data
- [ ] PostgreSQL support
- [ ] docker image deployment

### What could be added
list of features could not be certainly implemented

- [ ] other database support
- [ ] data export to PDF for print
- [ ] Personnalisable

## Contributing
This project is maintained by me, no long term support is guaranteed, but i am open for some help and ideas of functionality. contact me by email with pj.opensource@protonmail.ch

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
The project is licensed under the [GNU GPLv3](https://www.gnu.org/licenses/quick-guide-gplv3.html)
